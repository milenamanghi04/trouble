using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootPlayer : MonoBehaviour
{

    public GameObject Player;
    public Transform poscam;
    public Transform tirador;
    public float PlayerDistance = 2f;
    public float PlayerForce = 250f;
    private bool holdingPlayer = true;
    private Rigidbody PlayerRB;

    public GameObject cam;
    public Transform posPlayer;

    public float CamY;
    public float camZ;


    void Start()
    {
        PlayerRB = Player.GetComponent<Rigidbody>();
        PlayerRB.useGravity = false;
    }

    
    void Update()
    {

        

        if(holdingPlayer == true)
        {
         Player.transform.position = poscam.position + poscam.forward * PlayerDistance;

          if(Input.GetMouseButtonDown(0))
          {
                holdingPlayer = false;
            PlayerRB.useGravity = true;
            PlayerRB.AddForce(tirador.forward * PlayerForce);
          }

        }
        if(holdingPlayer == false)
        {

            float distancia = posPlayer.position.x;
            cam.transform.position = new Vector3(distancia,CamY,camZ);
        }

    }
}
